<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php"); ?>
    <title>Accès refusé - ECE Network</title>
  </head>

  <body class="no_access">
    <div class="login_container">
      <div class="login_panel panel-default col-lg-4 col-md-6 col-sm-6">
        <div class="no_access_title text-center">Oups !</div>
        <div class="no_access_body text-center">
          <h3>Il faut que vous soyez connecté pour acceder à cette page.</h3>
          <a href="connexion.php" class="btn btn-primary btn-lg" role="button">Se connecter</a>
        </div>
      </div>
    </div>
  </body>
</html>
