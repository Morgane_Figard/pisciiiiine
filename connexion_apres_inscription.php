<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php"); ?>
    <title>Connexion - ECE Network</title>
  </head>

  <body class="connexion">
    <div class="login_container">
      <div class="login_panel panel-default col-lg-2 col-md-4 col-sm-4">
        <div class="login_title text-center">Inscription réussie !</div>
        <div class="login_body text-center">
          <form action="controleur_connexion.php" method="post">
            <div class="form-group">
              <label for="ids">Identifiant :</label>
              <input type="text" required="true" class="form-control" name="ids" id="ids" placeholder="Nom d'utilisateur">
            </div>
            <div class="form-group">
              <label for="pwd">Mot de passe :</label>
              <input type="password" required="true" class="form-control" id="pwd" name="pwd">
            </div>
            <button type="submit" class="btn btn-primary btn-lg">Se connecter</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
