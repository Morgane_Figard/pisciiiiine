<?php
session_start();

try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$id_current_user = htmlspecialchars($_GET['id_current_user']);
$id_ami = htmlspecialchars($_GET['id_ami']);

$reqAmitie = $bdd->prepare('UPDATE amis SET statut=1,user_action=:userAction WHERE fk1=:id_current_user AND fk2=:id_ami OR fk1=:id_ami AND fk2=:id_current_user');


$reqAmitie->execute(array(
	'userAction' => $id_current_user,
	'id_current_user' => $id_current_user,
	'id_ami' => $id_ami
));

header("refresh:0;url=reseau.php");


?>