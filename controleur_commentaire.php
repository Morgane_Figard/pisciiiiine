<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$pseudo = $_SESSION['pseudo'];

$req_finduser = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo');
$req_finduser->execute(array(
    'pseudo' => $pseudo
	));

$user = $req_finduser->fetch();


$id_post = $_GET['id_post'];
$comment_contenu = isset($_POST["comment"])?$_POST["comment"] : "";
$time_stamp = date('Y-m-d H:i:s');


$req_commentaire= $bdd->prepare('INSERT INTO commentaire(id_auteur, id_post, contenu, time_stamp) VALUES(:id_auteur, :id_post, :contenu, :time_stamp)');
$req_commentaire->execute(array(
	'id_auteur' => $user['id'],
	'id_post' => $id_post,
	'contenu' => $comment_contenu,
	'time_stamp' => $time_stamp
));

header("refresh:0;url=index.php");

?>