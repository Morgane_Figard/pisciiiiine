<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

//empêche les failles XSS
$ids = htmlspecialchars($_POST['ids']);

$req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :ids');
$req->execute(array(
'ids' => $ids));

$resultat = $req->fetch();

if(!$resultat) {
	//echo 'mauvais pseudo';
	//echo 'Wrong username or password <br />';
	//header("refresh:3;url=login.php");
	header("refresh:0;url=connexion_wrong_login.php");
}
else {
	if(password_verify($_POST['pwd'], $resultat['password'])) {
		$_SESSION['pseudo'] = $ids;
		$_SESSION['pwd'] = $resultat['password'];
		$pseudo = $_SESSION['pseudo'];
		$pwd = $_SESSION['pwd'];

		$req2 = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
		$req2->execute(array(
		  'pseudo' => $pseudo,
		  'pwd' => $pwd));

		$user = $req2->fetch();
		$id_auteur = $user['id'];
		$last_co = date('Y-m-d');

		$reponse = $bdd->query("UPDATE auteur SET last_co = '$last_co' WHERE id = '$id_auteur'");

		header("refresh:0;url=index.php");
		//echo "T'es co!!!!!!!!!!! bg ";
	}
	else {
		//echo 'mauvais pwd';
		//echo 'Wrong username or password <br />';
		header("refresh:0;url=connexion_wrong_login.php");
	}
	
}

$req->closeCursor();

?>