<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$id_formation = htmlspecialchars($_GET['id_formation']);

$req_findformation = $bdd->prepare('DELETE FROM formation WHERE id_formation = :id_formation');
$req_findformation->execute(array(
    'id_formation' => $id_formation
	));

header("refresh:0;url=modifier_profil.php");
?>