<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$id_metier = htmlspecialchars($_GET['id_metier']);

$req_findmetier = $bdd->prepare('DELETE FROM metier WHERE id_metier = :id_metier');
$req_findmetier->execute(array(
    'id_metier' => $id_metier
	));

header("refresh:0;url=modifier_profil.php");
?>