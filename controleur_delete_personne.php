<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$id = htmlspecialchars($_GET['id']);

$req_pers = $bdd->prepare('DELETE FROM auteur WHERE id = :id');
$req_pers->execute(array(
    'id' => $id
	));
$req_ami1 = $bdd->prepare('DELETE FROM `amis` WHERE fk1 = :id');
$req_ami1->execute(array(
    'id' => $id
	));
$req_ami2 = $bdd->prepare('DELETE FROM `amis` WHERE fk2 = :id');
$req_ami2->execute(array(
    'id' => $id
	));

	$req_com = $bdd->prepare('DELETE FROM `commentaire` WHERE id_auteur = :id');
$req_com->execute(array(
    'id' => $id
	));
	
	$req_emploi = $bdd->prepare('DELETE FROM `emploi` WHERE id_auteur = :id');
$req_emploi->execute(array(
    'id' => $id
	));
	
	$req_forma = $bdd->prepare('DELETE FROM `formation` WHERE id_auteur = :id');
$req_forma->execute(array(
    'id' => $id
	));
	
	$req_like = $bdd->prepare('DELETE FROM `likes` WHERE id_auteur = :id');
$req_like->execute(array(
    'id' => $id
	));
	
	$req_metier = $bdd->prepare('DELETE FROM `metier` WHERE id_auteur = :id');
$req_metier->execute(array(
    'id' => $id
	));
	
$req_post = $bdd->prepare('DELETE FROM `post` WHERE id_auteur = :id');
$req_post->execute(array(
    'id' => $id
	));
header("refresh:0;url=formulaireadmin.php");
?>