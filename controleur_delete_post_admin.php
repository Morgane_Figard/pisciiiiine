<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$id_post = htmlspecialchars($_GET['id_post']);

$req_findpost = $bdd->prepare('DELETE FROM post WHERE id_post = :id_post');
$req_findpost->execute(array(
    'id_post' => $id_post
	));

header("refresh:0;url=formulaireadmin.php");
?>