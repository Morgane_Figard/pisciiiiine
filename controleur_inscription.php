<?php
//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}


$prenom = htmlspecialchars($_POST['prenom']);
$nom = htmlspecialchars($_POST['nom']);
$email = htmlspecialchars($_POST['email']);
$pseudo = htmlspecialchars($_POST['pseudo']);
$pwd1 = htmlspecialchars($_POST['pwd1']);
$pwd2 = htmlspecialchars($_POST['pwd2']);

$current_date = date('Y-m-d');
$bg_img = "prod/img/background/hypnotize.png";
$pp_img = "prod/img/default/user.png";
$cv_img = "prod/img/default/cv_default.png";

//si les mots de passe entrés sont différents
if($pwd2 != $pwd1) {
	echo 'The two passwords need to be identical!';
	//header("refresh:3;url=inscription.php");
}
else {
	//on vérifie que le username est unique
	$prereq = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo');
	$prereq->execute(array('pseudo' => $pseudo));

	$resultat = $prereq->fetch();

	//si l'username est bien unique, on insère les nouvelles données dans la BDD
	if(!$resultat) {
		$req = $bdd->prepare('INSERT INTO auteur(admin, email, pseudo, last_co, prenom, nom, password, bg_img, pp_img, cv_img,resume) VALUES(0, :email, :pseudo, :last_co, :prenom, :nom, :password, :bg_img, :pp_img, :cv_img, "")');
		$req->execute(array(
			'email' => $email,
			'pseudo' => $pseudo,
			'last_co' => $current_date,
			'prenom' => $prenom,
			'nom' => $nom,
			'password' => password_hash($pwd1, PASSWORD_DEFAULT),
			'bg_img' => $bg_img,
			'pp_img' => $pp_img,
			'cv_img' => $cv_img));
		//echo 'Account successfully created!';
		header("refresh:0;url=connexion_apres_inscription.php");
		$req->closeCursor();
	}
	else {
		//echo 'This username is already taken.';
		header("refresh:0;url=inscription_username.php?prenom=$prenom&nom=$nom&email=$email&pseudo=$pseudo");
	}
	$prereq->closeCursor();
}
?>