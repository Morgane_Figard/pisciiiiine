<?php

session_start();

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$pseudo = $_SESSION['pseudo'];

$req_finduser = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo');
$req_finduser->execute(array(
    'pseudo' => $pseudo
	));

$user = $req_finduser->fetch();


if($_FILES["image"]["name"])
{
$target_dir = "prod/img/media/";
$target_file = $target_dir .basename($_FILES["image"]["name"]);
$pp_img = $target_file;
}
else
{
	$pp_img="prod/img/default/user.png";
}


$id_auteur = $user['id'];
$prenom = isset($_POST["prenom"])?$_POST["prenom"] : "";
$nom = isset($_POST["nom"])?$_POST["nom"] : "";
$resume = isset($_POST["resume"])?$_POST["resume"] : "";


$reponse = $bdd->prepare("UPDATE auteur SET prenom = :prenom ,nom = :nom ,  resume= :resume, pp_img= :pp_img WHERE id = :id_auteur");
$reponse->execute(array(
	'prenom' => $prenom,
	'nom' => $nom,
	'resume' => $resume,
	'pp_img' => $pp_img,
	'id_auteur' => $id_auteur
));

/*if(!$reponse) {
	echo "requete non effectuee";
}
else {
	echo "cc";
}*/
header("refresh:0;url=modifier_profil.php");

?>