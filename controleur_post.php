<?php

session_start();

$contenu_texte = htmlspecialchars($_POST['text_post']);

//connexion à la base de données
try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$pseudo = $_SESSION['pseudo'];
//echo $pseudo . '<br />';

//echo $_FILES["image"]["name"];
//dossier image
if($_FILES["image"]["name"])
{
$target_dir = "prod/img/media/";
$target_file = $target_dir .basename($_FILES["image"]["name"]);
$contenu_media = $target_file;
}
else
{
	$contenu_media="";
}


$req_finduser = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo');
$req_finduser->execute(array(
    'pseudo' => $pseudo
	));

$user = $req_finduser->fetch();

$id_auteur = $user['id'];
$current_date = date('Y-m-d H:i:s');


if($_POST['lieu_post']!="") {
	$lieu_post = ' - à ' . htmlspecialchars($_POST['lieu_post']);
}
else {
	$lieu_post = "";
}

$req = $bdd->prepare('INSERT INTO post(id_auteur, contenu_texte, date_post, contenu_media, lieu_post, visibilite) VALUES(:id_auteur, :contenu_texte, :date_post, :contenu_media, :lieu_post, 0)');
	$req->execute(array(
		'id_auteur' => $id_auteur,
		'contenu_texte' => $contenu_texte,
		'date_post' => $current_date,
		'contenu_media' => $contenu_media,
		'lieu_post' => $lieu_post));
	//echo 'Post inséré inchallah <br />';
	//echo $id_auteur . ' ' . $contenu_texte . ' ' . $current_date . ' ' . $contenu_media . ' ' . $lieu_post;
	header("refresh:0;url=index.php");
	$req->closeCursor();

?>