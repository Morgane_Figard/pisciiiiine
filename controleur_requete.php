<?php
session_start();

try {
	$bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
}
catch (Exception $e) {
	die('Erreur : ' . $e->getMessage());
}

$id_current_user = htmlspecialchars($_GET['id_current_user']);
$id_utilisateur = htmlspecialchars($_GET['id_utilisateur']);



$reqAmitie = $bdd->prepare('INSERT INTO amis (statut,fk1,fk2,user_action) VALUES(0,:id_current_user,:id_utilisateur,:id_current_user)');

$reqAmitie->execute(array(
	'id_current_user' => $id_current_user,
	'id_utilisateur' => $id_utilisateur
));

header("refresh:0;url=reseau.php");


?>