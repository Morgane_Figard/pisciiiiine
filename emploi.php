<!-- vérification de la connexion de l'utilisateur -->
<?php 
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();
  $req_notif = $bdd->prepare("SELECT count(statut) FROM amis WHERE (fk1=:id_current_user OR fk2=:id_current_user) AND statut=0 AND user_action!=:id_current_user");
  $req_notif->execute(array(
  'id_current_user' => $user['id']));
  $nbre_notif = $req_notif->fetch();

    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
      <?php include("head.php"); ?>
    <title>Réseau - ECE Network</title>
  </head>

  <!--<body class="nav-md">-->
  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
          <!-- top navigation -->
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="index.php" class="nav-text">Accueil</a></li>
                <li><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li class="active"><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="notifications.php" class="nav-text"><i class="fa fa-bell-o"></i>
                
                <?php 
                if($nbre_notif['count(statut)'] != 0)
                {
                  ?>
                <span class="badge badge-notify"> <?php echo $nbre_notif['count(statut)'] ?></span></a></li>
                <?php
                }
                ?>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="profil.php" class="nav-text">Mon profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- /top navigation -->
          <!-- page content -->
        <div class="container-fluid">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <form  method="post" class="navbar-form">
                <div class="list-group-item">
                  <p>Rechercher par...</p>
                  <label class="radio-inline"><input type="radio" name="optradio" value = "Intitulé" checked="checked">Intitulé</label>
                  <label class="radio-inline"><input type="radio" name="optradio" value = "Entreprise">Entreprise</label>
                </div>
                <div class="input-group col-xs-12">
                  <input class="form-control" placeholder="Rechercher un emploi..." name="srch-term" id="srch-term" type="text">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
    <?php

    $answer = isset($_POST['optradio'])?$_POST["optradio"]: "";

    if ($answer == "Intitulé") {          
        
    $recherche = 'Intitulé';    
    }
    else if($answer == "Entreprise")
    {
        
        $recherche = 'Entreprise';
    }
        
        $srchterm = isset($_POST["srch-term"])?$_POST["srch-term"] : "";

        $idauteur = $user['id'];
    
    
    
    
        if(empty($srchterm )){

          $req = $bdd->query("SELECT * FROM emploi");

          while($post = $req->fetch()){
          ?>
            <div class="container-fluid">
              <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <div class="list-group-item">
                    <?php
                      echo "Intitule : ";
                      echo $post['intitule'];
                      echo " <br>";
                      echo "Description: ";
                      echo $post['description']; 
                      echo "<br>";
                      echo "Salaire : ";
                      echo $post['salaire']; 
                      echo "<br>";
                      echo "Entreprise : ";
                      echo $post['entreprise']; 
                    
                    ?>
                  </div>
                </div>
              </div>
            </div>   
          <?php
          }
        }
           
        else if($answer == "Intitulé"){
              
          $req = $bdd->query("SELECT * FROM emploi WHERE `intitule` LIKE '%$srchterm%' ");
          
          while($post = $req->fetch()){
            ?>
            <div class="container-fluid">
              <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <div class="list-group-item">
                    <?php
                    echo "Intitule : ";
                    echo $post['intitule'];
                    echo " <br>";
                    echo "Description: ";
                    echo $post['description']; 
                    echo "<br>";
                    echo "Salaire : ";
                    echo $post['salaire']; 
                    echo "<br>";
                    echo "Entreprise : ";
                    echo $post['entreprise']; 
                    
                    ?>
                  </div>
                </div>
              </div>
            </div> 
          <?php
          }
        }   
            

    
          else if($answer == "Entreprise"){
                
            $req = $bdd->query("SELECT * FROM emploi WHERE `entreprise`  LIKE '%$srchterm%'");
            while($post = $req->fetch()){
              ?>
              <div class="container-fluid">
                <div class="col-md-8 col-md-offset-2">
                  <div class="panel panel-default">
                    <div class="list-group-item">
                      <?php
                      echo "Intitule : ";
                      echo $post['intitule'];
                      echo " <br>";
                      echo "Description: ";
                      echo $post['description']; 
                      echo "<br>";
                      echo "Salaire : ";
                      echo $post['salaire']; 
                      echo "<br>";
                      echo "Entreprise : ";
                      echo $post['entreprise']; 
                      ?>
                    </div>
                  </div>
                </div>
              </div>  
            <?php
            }
          }
          ?> 
          <!-- /page content -->
      </div>
    </div>
  </body>
  </html>
  <?php 
 
  }

  ?>