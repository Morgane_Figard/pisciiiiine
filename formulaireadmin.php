<!-- vérification de la connexion de l'utilisateur -->
<?php 
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();

  if($user['admin']==1) {

    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include("head.php"); ?>
    <title>Réseau - ECE Network</title>
  </head>

  <!--<body class="nav-md">-->
  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
      	<!-- top navigation -->
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="index.php" class="nav-text">Accueil</a></li>
                <li><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="nav-text"><i class="fa fa-bell-o"></i></a></li>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="profil.php" class="nav-text">Profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
        </nav>
		
<h2>Ajouter un utilisateur</h2>

<form  method="post">

L'utilisateur est il un admin?
<select name="taskOption">
  <option value="oui">OUI</option>
  <option value="non">NON</option>

</select>
<br>
  Email:<br>
  <input type="text" name="email" required ="true">
  <br>
  Pseudo:<br>
  <input type="text" name="pseudo" required ="true">
  <br>
  Prenom:<br>
  <input type="text" name="prenom" required ="true">
  <br>
  
  Nom:<br>
  <input type="text" name="nom" required ="true">
  <br>
  Password:<br>
  <input type="password" name="password" required ="true">
  <br>
<button class="btn btn-default" type="submit">Ajouter</button>
</form>

	<?php

	
	if(isset($_POST['taskOption'])){
		
		$answer = $_POST['taskOption'];
	

	if ($answer == "oui") {          
    
$recherche = 1;	
}
else if($answer == "non")
{
    $recherche = 0;
}


	$email = isset($_POST["email"])?$_POST["email"] : "";
	$pseudo = isset($_POST["pseudo"])?$_POST["pseudo"] : "";
	$prenom = isset($_POST["prenom"])?$_POST["prenom"] : "";
	$nom = isset($_POST["nom"])?$_POST["nom"] : "";
	$passwordd = isset($_POST["password"])?$_POST["password"] : "";
	$current_date = date('Y-m-d');
	$bg_img = "prod/img/background/hypnotize.png";
	$pp_img = "prod/img/default/user.png";
	$cv_img = "prod/img/default/cv_default.png";
	
	$password = password_hash($passwordd, PASSWORD_DEFAULT);
	
	//on vérifie que le username est unique
	$prereq = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo');
	$prereq->execute(array('pseudo' => $pseudo));

	$resultat = $prereq->fetch();

	//si l'username est bien unique, on insère les nouvelles données dans la BDD
	if(!$resultat) {
		
		$req = $bdd->query("INSERT INTO auteur(admin, email, pseudo, last_co, prenom, nom, password, bg_img, pp_img, cv_img,resume) VALUES( '$recherche', '$email', '$pseudo', '$current_date', '$prenom', '$nom', '$password', '$bg_img', '$pp_img', '$cv_img', '')");
		
	}
	else {
		echo 'This username is already taken.';
	}
	$prereq->closeCursor();
	
	}
    
	?>

<h2>Supprimer un utilisateur</h2>

<form  method="post">

<input class="form-control" placeholder="Rechercher un pseudo..." name="srch-term" id="srch-term" type="text">
 <input type="submit" value="Submit">
</form>



<?php

	$srchterm = isset($_POST["srch-term"])?$_POST["srch-term"] : "";
	
	
	if(empty($srchterm )){
				
		$req2 = $bdd->query("SELECT * FROM auteur");
	
		while($morjeanne = $req2->fetch()){
			?>
			
			
			
			<div class="list-group-item">
			<?php
			echo "Pseudo : ";
		echo $morjeanne['pseudo'];
		echo " <br>";
		echo "Prenom: ";
		 echo $morjeanne['prenom']; 
		 echo "<br>";
		 echo "Nom : ";
		 echo $morjeanne['nom']; 
		 echo "<br>";
		 ?>
		 <a href="controleur_delete_personne.php?id=<?php echo $morjeanne['id']?>"><i class="fa fa-trash-o"></i></a>
		 <br>
		 </div>
		 <?php
		
		}
			
	}
	else{
		
		$req2 = $bdd->query("SELECT * FROM auteur WHERE `pseudo` LIKE '%$srchterm%'");
		
		while($morjeanne = $req2->fetch()){
			?>
			
			
			
			<div class="list-group-item">
			<?php
			echo "Pseudo : ";
		echo $morjeanne['pseudo'];
		echo " <br>";
		echo "Prenom: ";
		 echo $morjeanne['prenom']; 
		 echo "<br>";
		 echo "Nom : ";
		 echo $morjeanne['nom']; 
		 echo "<br>";
		 ?>
		 <a href="controleur_delete_personne.php?id=<?php echo $morjeanne['id']?>"><i class="fa fa-trash-o"></i></a>

		 <br>
		 </div>
		 <?php

		
		}
		
	}
?>


<h2>Supprimer un post</h2>

<form  method="post">
<label class="radio-inline"><input type="radio" name="optradio" value = "mots" checked="checked">Mots-clefs</label>
<label class="radio-inline"><input type="radio" name="optradio" value = "pseudo">Pseudo</label>
<input class="form-control" placeholder="Recherche..." name="srch-term2" id="srch-term2" type="text">
 <input type="submit" value="Submit">
</form>

</body>
</html>

<?php

$idauteur = $user['id'];
$answer2 = isset($_POST['optradio'])?$_POST["optradio"]: "";
$srchterm2 = isset($_POST["srch-term2"])?$_POST["srch-term2"] : "";

if ($answer2 == "mots") {          
    
$recherche2 = 'contenu_texte';	
}
else if($answer2 == "pseudo")
{
	
    $recherche2 = $bdd->query("SELECT id FROM auteur WHERE `pseudo` LIKE '%$srchterm2%'");
}
	
	

	if(empty($srchterm2)){
				
		$req3 = $bdd->query("SELECT * FROM post");
	
		while($morjeanne = $req3->fetch()){
			?>

			<div class="list-group-item">
			<?php
			$idd = $morjeanne['id_auteur'];

			 $recherchepseudo = $bdd->query("SELECT * FROM auteur WHERE `id`='$idd'");
			 $mor = $recherchepseudo->fetch();
			echo "Pseudo : ";
			$pse = $mor['pseudo'];
			echo $pse;
			echo '<br>';
			echo 'Post: ';
		echo $morjeanne['contenu_texte'];
		echo " <br>";
		echo "Date : ";
		 echo $morjeanne['date_post']; 
		 echo "<br>";
		 
		 ?>
		 <a href="controleur_delete_post_admin.php?id_post=<?php echo $morjeanne['id_post']?>"><i class="fa fa-trash-o"></i></a>
		 <br>
		 </div>
		 <?php
		
		}
			
	}
else if($answer2 == 'mots'){
		
		$req3 = $bdd->query("SELECT * FROM post WHERE `contenu_texte` LIKE '%$srchterm2%'");
	
		while($morjeanne = $req3->fetch()){
			?>

			<div class="list-group-item">
			<?php
			$idd= $morjeanne['id_auteur'];
			$recherchepseudo = $bdd->query("SELECT * FROM auteur WHERE `id`='$idd'");
			 $mor = $recherchepseudo->fetch();
			echo "Pseudo : ";
			echo $mor['pseudo'];
			echo '<br>';
			echo 'Post: ';
		echo $morjeanne['contenu_texte'];
		echo " <br>";
		echo "Date : ";
		 echo $morjeanne['date_post']; 
		 echo "<br>";
		 ?>
		 <a href="controleur_delete_post_admin.php?id_post=<?php echo $morjeanne['id_post']?>"><i class="fa fa-trash-o"></i></a>
		 <br>
		 </div>
		 <?php
		
		
	}
	}
else if($answer2 == 'pseudo'){
		$idd = $bdd->query("SELECT * FROM auteur WHERE `id_auteur` LIKE '%$srchterm2%'");
		$req3 = $bdd->query("SELECT * FROM post WHERE `id_auteur` LIKE '%$srchterm2%'");
		
		while($morjeanne = $req3->fetch()){
			?>

			<div class="list-group-item">
			<?php
			$idd=$morjeanne['id_auteur'];
			 $recherchepseudo = $bdd->query("SELECT * FROM auteur WHERE `id`='$idd'");
			 $mor = $recherchepseudo->fetch();
			echo "Pseudo : ";
			echo $mor['pseudo'];
			echo '<br>';
			echo 'Post: ';
		echo $morjeanne['contenu_texte'];
		echo " <br>";
		echo "Date : ";
		 echo $morjeanne['date_post']; 
		 echo "<br>";
		 ?>
		 <a href="controleur_delete_post_admin.php?id_post=<?php echo $morjeanne['id_post']?>"><i class="fa fa-trash-o"></i></a>
		 <br>
		 </div>
		 <?php
		
	}
	}
?>


<h2>Ajouter un emploi</h2>

<form  method="post">

Type d'emploi:
<select name="taskOption2">
  <option value="oui">Offre</option>
  <option value="non">Demande</option>

</select>
<br>
  Intitule:<br>
  <input type="text" name="intitule" required ="true">
  <br>
  Description:<br>
  <input type="text" name="description" required ="true">
  <br>
  Salaire:<br>
  <input type="text" name="salaire" required ="true">
  <br>
  
  Entreprise:<br>
  <input type="text" name="entreprise" required ="true">
  <br>

<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
</form>
</body>
</html>


<?php

	
	if(isset($_POST['taskOption2'])){
		
		$answer = $_POST['taskOption2'];
	

	if ($answer == "oui") {          
    
$type = 0;	
}
else if($answer == "non")
{
    $type = 1;
}


	$intitule = isset($_POST["intitule"])?$_POST["intitule"] : "";
	$description = isset($_POST["description"])?$_POST["description"] : "";
	$salaire = isset($_POST["salaire"])?$_POST["salaire"] : "";
	$entreprise = isset($_POST["entreprise"])?$_POST["entreprise"] : "";

		
	$req5 = $bdd->query("INSERT INTO `emploi`(`id_auteur`, `type`, `intitule`, `description`, `salaire`, `entreprise`) VALUES ('$idauteur','$type','$intitule','$description','$salaire','$entreprise')");

	}

?>
  <?php 
 }
 else {
 	include("probleme.php");
 }
  }

  ?>
