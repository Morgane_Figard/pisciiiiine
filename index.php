<!-- vérification de la connexion de l'utilisateur -->
<?php 
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();
  $req_notif = $bdd->prepare("SELECT count(statut) FROM amis WHERE (fk1=:id_current_user OR fk2=:id_current_user) AND statut=0 AND user_action!=:id_current_user");
  $req_notif->execute(array(
  'id_current_user' => $user['id']));
  $nbre_notif = $req_notif->fetch();
                

    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include("head.php"); ?>
    <title>Accueil - ECE Network</title>
    <style type="text/css">
      .label-file {
          cursor: pointer;
          color: #grey;
          font-weight: bold;
      }
      .label-file:hover {
          color: #grey;
      }

      
      
  </style>
  </head>

  <!--<body class="nav-md">-->
  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
      	<!-- top navigation -->
      	<?php //include("top_nav.php"); ?>
        
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php" class="nav-text">Accueil</a></li>
                <li><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="notifications.php" class="nav-text"><i class="fa fa-bell-o"></i>
                
                <?php 
                if($nbre_notif['count(statut)'] != 0)
                {
                  ?>
                <span class="badge badge-notify"> <?php echo $nbre_notif['count(statut)'] ?></span></a></li>
                <?php
                }
                ?>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="profil.php" class="nav-text">Mon profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
        </nav>
        <!--<div class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php"><span class="nav-text">Accueil</span></a></li>
                <li><a href="#" class="nav-text">Réseau</a></li>
                <li><a href="#" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="nav-text"><i class="fa fa-bell-o"></i></a></li>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="#" class="nav-text">Profil</a></li>
              </ul>
            </div>
          </div>
        </div>-->
        <!-- /top navigation -->
      	<!-- page content -->
        <div class="container-fluid">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <!--<div class="panel-heading">
                              <h3 class="panel-title">Titre :</h3>
              </div>-->
              <!--<div class="list-group panel-body">
                <div class="form-group list-group-item">
                  <textarea class="form-control" rows="5" id="comment" placeholder="A quoi pensez-vous?"></textarea>
                </div>
                <div class="list-group-item">
                  <button class="btn-default">Média</button>
                </div>
              </div>-->
              <form action="controleur_post.php" method="post" enctype="multipart/form-data">
                <div class="form-group list-group-item">
                  <label>Bonjour, <?php echo $user['prenom']; ?> !</label>
                </div>
                <div class="form-group list-group-item">
                  <textarea class="form-control" rows="5" id="text_post" placeholder="A quoi pensez-vous?" name="text_post"></textarea>
                </div>
                <p class="list-group-item">
                  <button class="btn btn-default " type="button"><label for="image" class="label-file"><i class="fa fa-paperclip"></i> Média</label></button>
                  <input id="image" name="image" style="display: none;" type="file">
                  <span class="col-xs-4"><input class="form-control" placeholder="Ajouter un lieu" name="lieu_post" id="lieu_post" type="text"></span>
                  <button type="submit" class="btn btn-primary pull-right">Publier</button>
                </p>
              </form>
            </div>
          </div>
        </div>
        <?php
          //$id_auteur = $user['id'];
          /*$req_post = $bdd->prepare('SELECT * FROM post WHERE id_auteur = :id_auteur ORDER BY date_post DESC');
          $req_post->execute(array(
              'id_auteur' => $id_auteur));*/
          $req_post = $bdd->query('SELECT * FROM post ORDER BY date_post DESC');

          while($post = $req_post->fetch()) {
            $id_auteur = $post['id_auteur'];
            $req_auteur = $bdd->prepare('SELECT * FROM auteur WHERE id = :id_auteur');
            $req_auteur->execute(array('id_auteur' => $id_auteur));
            $auteur = $req_auteur->fetch();
            $check = $bdd->prepare('SELECT * FROM amis WHERE fk1=:id_current_user AND fk2=:id_auteur
            OR fk1=:id_auteur AND fk2=:id_current_user');
            $check->execute(array(
              'id_current_user' => $user['id'],
              'id_auteur' => $id_auteur));
            $check_friend = $check->fetch();
            $id_post = $post['id_post'];
      $check1 = $bdd->prepare('SELECT * FROM likes WHERE id_post = :id_post');
            $check1->execute(array(
              'id_post' => $id_post));
      $comptlike = 0;
      while($check_like = $check1->fetch()){
        $comptlike++;
      }
            ?>
              <div class="container-fluid">
                <div class="col-md-8 col-md-offset-2">
                  <?php
                  if($check_friend['statut']==1 || $id_auteur==$user['id'])
                    {
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title"><img src="<?php echo $auteur['pp_img'] ?>" height="50" width="50" style="border-radius: 50%;"> <a href="profil_ami.php?id_ami=<?php echo $auteur['id']; ?>"><?php echo $auteur['prenom']." ".$auteur['nom']; ?></a>
					  <a href="controleur_repost.php?id_post=<?php echo $post['id_post']?>"><span> </span><i class="glyphicon glyphicon-retweet pull-right" style="margin-left: 10px;"></i></a>
            <?php 
            
            if($comptlike != 0){

            ?>
            <span class="badge badge-notify pull-right"> <?php echo $comptlike ?></span></a></li>
            <?php
            }
            ?>
            <a href="controleur_like.php?id_post=<?php echo $post['id_post']?>"><span> </span><i class="fa fa-thumbs-up pull-right"></i></a>
                        <?php
                          $req_findauteur = $bdd->prepare('SELECT * FROM post WHERE id_post = :id_post AND id_auteur = :id_auteur');
                          $req_findauteur->execute(array(
                              'id_post' => $post['id_post'],
                              'id_auteur' => $user['id']
                            ));

                          $resultat = $req_findauteur->fetch();
                          if($resultat) {
                            ?>
                        <a href="controleur_delete_post.php?id_post=<?php echo $post['id_post']?>"><i class="fa fa-trash-o pull-right" style="margin-right: 10px;"></i></a><?php } ?></h3>
                    </div>
                    <div class="list-group-item">
                      <?php
                     if($post['contenu_media']!="")
                     {
                      ?>
                      <p> <img src="<?php echo $post['contenu_media'] ?>" heigth="800" width="600" class="img-responsive"></p>
                      <?php
                    }
                    ?>
                      
                      <p><?php echo $post['contenu_texte']; ?></p>
                      <h6><?php echo $post['date_post'] . $post['lieu_post']; ?></h6>
                    </div>

                      <div class="list-group-item">
                        <form action="controleur_commentaire.php?id_post=<?php echo $post['id_post']; ?>" method="post">
                        <input class="form-control" placeholder="Ajouter un commentaire" name="comment" id="comment" type="text">
                        <button type="submit" class="btn btn-info">Commenter</button>                      
                        </form>
                      </div>

                      <?php 
                      $req_commentaire = $bdd->prepare('SELECT * FROM commentaire WHERE id_post=:id_post');
                      $req_commentaire->execute(array(
                          'id_post' => $post['id_post']
                      ));
                     

                      while($comment = $req_commentaire->fetch())
                      {
                        $req_auteur_com = $bdd->prepare('SELECT * FROM auteur WHERE id=:id_auteur');
                        $req_auteur_com->execute(array(
                          'id_auteur' => $comment['id_auteur']
                      ));
                        $auteur_com = $req_auteur_com->fetch();
                      ?>

                      <div class="list-group-item">
                        <h5><u><?php echo $auteur_com['prenom'] . ' ' . $auteur_com['nom'];?></u></h5>
                        <h5> <?php echo $comment['contenu']; ?> </h5>
                        <h6> <?php echo $comment['time_stamp'] ?> </h6>
                      </div>
                    <?php
                    }
                    ?>
                  </div>
                <?php
                }
                ?>
                </div>
              </div>
            <?php
          }
        ?>
        <!--<div class="container-fluid">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Auteur du post</h3>
              </div>
              <div class="list-group-item">
                <p>Contenu du post</p>
              </div>
            </div>
          </div>
        </div>-->
        <!-- /page content -->
      </div>
    </div>
  </body>
  </html>

<?php 
 
  }

  ?>