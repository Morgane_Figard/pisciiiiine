<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php"); ?>
    <title>Inscription - ECE Network</title>
  </head>

  <body class="connexion">
    <div class="login_container">
      <div class="login_panel panel-default col-lg-2 col-md-4 col-sm-4">
        <div class="login_title text-center">Inscription</div>
        <div class="login_body text-center">
          <form action="controleur_inscription.php" method="post">
            <div class="form-group">
              <label for="prenom">Prénom* :</label>
              <input type="text" required="true" class="form-control" name="prenom" id="prenom">
            </div>
            <div class="form-group">
              <label for="nom">Nom* :</label>
              <input type="text" required="true" class="form-control" name="nom" id="nom">
            </div>
            <div class="form-group">
              <label for="email">Email* :</label>
              <input type="email" required="true" class="form-control" name="email" id="email">
            </div>
            <div class="form-group">
              <label for="pseudo">Nom d'utilisateur* :</label>
              <input type="text" required="true" class="form-control" name="pseudo" id="pseudo">
            </div>
            <div class="form-group">
              <label for="pwd">Mot de passe* :</label>
              <input type="password" required="true" class="form-control" name="pwd1" id="pwd1">
            </div>
            <div class="form-group">
              <label for="pwd2">Retapez votre mot de passe* :</label>
              <input type="password" required="true" class="form-control" name="pwd2" id="pwd2">
            </div>
            <button type="submit" class="btn btn-primary btn-lg">S'inscrire</button>
          </form>
          <h6><br /></h6>
          <h5>Déjà inscrit ? <a href="connexion.php" class="btn btn-default " role="button">Se connecter</a></h5>
        </div>
      </div>
    </div>
  </body>
</html>
