<!-- vérification de la connexion de l'utilisateur -->
<?php 
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();
    $req_notif = $bdd->prepare("SELECT count(statut) FROM amis WHERE (fk1=:id_current_user OR fk2=:id_current_user) AND statut=0 AND user_action!=:id_current_user");
  $req_notif->execute(array(
  'id_current_user' => $user['id']));
  $nbre_notif = $req_notif->fetch();

    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include("head.php"); ?>
    <script type="text/javascript">
      function show_form_exp() {
        $("#add_exp").toggle(); 
      }
      function show_form_formation() {
        $("#add_formation").toggle(); 
      }
    </script>
    <title>Profil - ECE Network</title>
  </head>

  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
      	<!-- top navigation -->
        
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="index.php" class="nav-text">Accueil</a></li>
                <li><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="notifications.php" class="nav-text"><i class="fa fa-bell-o"></i>
                <?php 
                if($nbre_notif['count(statut)'] != 0)
                {
                  ?>
                <span class="badge badge-notify"> <?php echo $nbre_notif['count(statut)'] ?></span></a></li>
                <?php
                }
                ?>                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li class="active"><a href="profil.php" class="nav-text">Mon profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
        </nav>
        <!-- /top navigation -->
      	<!-- page content -->
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                Choisissez votre nouveau fond d'écran :
              </div>
              <div class="panel-body">
                <table class="table" style="border: none !important;">
                  <thead>
                    <tr>
                      <td style="text-align: center;">
                        <img src="prod/img/background/hypnotize.png" height="130" width="130" style="border-width: 1px; border-color: black; border-style: solid; margin-bottom: 10px;">
                        <br />
                        <span> <a href="controleur_modifier_background.php?nom_bg=prod/img/background/hypnotize.png" class="btn btn-primary" role="button" style="word-wrap:break-word; white-space: normal;">Choisir</a></span>
                      </td>
                      <td style="text-align: center;">
                        <img src="prod/img/background/memphis-colorful.png" height="130" width="130" style="border-width: 1px; border-color: black; border-style: solid; margin-bottom: 10px;">
                        <br />
                        <span> <a href="controleur_modifier_background.php?nom_bg=prod/img/background/memphis-colorful.png" class="btn btn-primary" role="button" style="word-wrap:break-word; white-space: normal;">Choisir</a></span>
                      </td>
                      <td style="text-align: center;">
                        <img src="prod/img/background/halftone-yellow.png" height="130" width="130" style="border-width: 1px; border-color: black; border-style: solid; margin-bottom: 10px;">
                        <br />
                        <span> <a href="controleur_modifier_background.php?nom_bg=prod/img/background/halftone-yellow.png" class="btn btn-primary" role="button" style="word-wrap:break-word; white-space: normal;">Choisir</a></span>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align: center;">
                        <img src="prod/img/background/sakura.png" height="130" width="130" style="border-width: 1px; border-color: black; border-style: solid; margin-bottom: 10px;">
                        <br />
                        <span> <a href="controleur_modifier_background.php?nom_bg=prod/img/background/sakura.png" class="btn btn-primary" role="button" style="word-wrap:break-word; white-space: normal;">Choisir</a></span>
                      </td>
                      <td style="text-align: center;">
                        <img src="prod/img/background/giftly.png" height="130" width="130" style="border-width: 1px; border-color: black; border-style: solid; margin-bottom: 10px;">
                        <br />
                        <span> <a href="controleur_modifier_background.php?nom_bg=prod/img/background/giftly.png" class="btn btn-primary" role="button" style="word-wrap:break-word; white-space: normal;">Choisir</a></span>
                      </td>
                      <td style="text-align: center;">
                        <img src="prod/img/background/cloudy-day.png" height="130" width="130" style="border-width: 1px; border-color: black; border-style: solid; margin-bottom: 10px;">
                        <br />
                        <span> <a href="controleur_modifier_background.php?nom_bg=prod/img/background/cloudy-day.png" class="btn btn-primary" role="button" style="word-wrap:break-word; white-space: normal;">Choisir</a></span>
                    </td>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <span style="text-align: center;"><a href="modifier_profil.php" class="btn btn-success form-control" role="button" style="word-wrap:break-word; white-space: normal;">Enregistrer et retourner à la page précédente</a></span>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
  </html>
<?php 
 
  }

  ?>