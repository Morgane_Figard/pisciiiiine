
<!-- vérification de la connexion de l'utilisateur -->
<?php
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();

    ?>

<html lang="en">
  <head>
  	<?php include("head.php"); ?>
    <title>Réseau - ECE Network</title>
  </head>

  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
      	<!-- top navigation -->
      	<?php //include("top_nav.php"); ?>
        
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="index.php" class="nav-text">Accueil</a></li>
                <li><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="notifications.php" class="nav-text"><i class="fa fa-bell-o"></i></a></li>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="profil.php" class="nav-text">Mon profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
        </nav>
        <!-- /top navigation -->
      	<!-- page content -->
        <div class="container-fluid">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            	<div class="list-group">
            		<?php
            		$id_notif =null;
					$check = $bdd->query('SELECT * FROM amis');
		            while($check_friend = $check->fetch())
		            {

		            	//on regarde si le statut de la requête est en attente, si l'utilisateur courant fait parti de la requête et si ce n'est pas lui qui l'a envoyée
		            	if($check_friend['statut']==0 && ($check_friend['fk1']==$user['id'] || $check_friend['fk2']==$user['id']) && $check_friend['user_action']!=$user['id'])
		            	{
		            		$id_notif = $check_friend['user_action'];
		            	
		            	$req_infos = $bdd->prepare('SELECT * FROM auteur WHERE id=:id_notif');
		            	$req_infos->execute(array(
		            		'id_notif' => $id_notif
		            	));
		            	$infos_notif = $req_infos->fetch();
		            	?>
		            	<p class="list-group-item align-middle" style="height: 60px; vertical-align: center;">  <?php  echo $infos_notif['prenom']." ".$infos_notif['nom']." vous a envoyé une demande d'ami !" ?>
 							 <a href="controleur_refuser_requete.php?id_current_user=<?php echo $user['id']?>&id_ami=<?php echo $infos_notif['id']?>" class="btn btn-danger pull-right" role="button">Refuser</a>

							 <a href="controleur_accepter_requete.php?id_current_user=<?php echo $user['id']?>&id_ami=<?php echo $infos_notif['id']?>" class="btn btn-success pull-right" role="button">Accepter</a>

	            		</p>
		            	
		            	<?php
		            	}
		            } 
		            if($id_notif == null)
		            {
		            	?>
		            	<p class="list-group-item">Vous n'avez pas de notifications.</p>
		            	<?php
		            }
		            ?>
		            	
		            
          		</div>
            </div>
          </div>
        </div>
      </div>
	</div>
</body>
</html>

<?php 
 
  }

  ?>