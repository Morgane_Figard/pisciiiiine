-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 03 mai 2018 à 14:22
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `piscine`
--

-- --------------------------------------------------------

--
-- Structure de la table `amis`
--

DROP TABLE IF EXISTS `amis`;
CREATE TABLE IF NOT EXISTS `amis` (
  `statut` int(11) NOT NULL,
  `fk1` int(11) NOT NULL,
  `fk2` int(11) NOT NULL,
  `user_action` int(11) NOT NULL,
  PRIMARY KEY (`fk1`,`fk2`),
  KEY `id1` (`fk1`),
  KEY `id2` (`fk2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `amis`
--

INSERT INTO `amis` (`statut`, `fk1`, `fk2`, `user_action`) VALUES
(1, 7, 9, 7),
(0, 7, 8, 7),
(1, 11, 7, 7),
(1, 7, 10, 7),
(1, 11, 12, 11),
(1, 11, 8, 11);

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

DROP TABLE IF EXISTS `auteur`;
CREATE TABLE IF NOT EXISTS `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` tinyint(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `last_co` date NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `bg_img` varchar(255) NOT NULL,
  `pp_img` varchar(255) NOT NULL,
  `cv_img` blob NOT NULL,
  `resume` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `auteur`
--

INSERT INTO `auteur` (`id`, `admin`, `email`, `pseudo`, `last_co`, `prenom`, `nom`, `password`, `bg_img`, `pp_img`, `cv_img`, `resume`) VALUES
(9, 0, 'jacob@gmail.yahoo.laposte.net', 'oui', '2018-05-02', 'Mr', 'ToutLeMonde', '$2y$10$qJuTtRoxY6.hG6Xpiaq.pu3oE0vxQ/uPSguPIekrBol/1MsxwC3X.', '../img/hypnotize.png', '../img/user.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, ''),
(7, 0, 'igor@gmail.com', 'Igzerrr', '2018-05-02', 'Igor', 'FIDALGO', '$2y$10$/GGhVBtxbptAY90b7txDt.PUvUMIGAfmELe7MdEv6dGYEFyLgCVf2', '../img/hypnotize.png', '../img/user.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, ''),
(8, 0, 'mlmdz@pop.fr', 'test', '2018-05-02', 'Igor', 'mlk', '$2y$10$MV.6RHBvkm/V/eYqzseHyOaW6Qxl/oXC1D3uGfJT7tCUfX9p99Gi.', '../img/hypnotize.png', '../img/user.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, ''),
(10, 0, 'thomas@yahoo.fr', 'Toms', '2018-05-03', 'thomas', 'Fidalgo', '$2y$10$Gohk9MD1FEdWOf6YLpaw8e3WG48vooW65zXP2f22B/UiESAAlVoJ.', '../img/hypnotize.png', '../img/user.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, ''),
(11, 0, 'morjeanne@ece.edu.fr', 'Momo', '2018-05-03', 'Morgane', 'Figard', '$2y$10$zqm4fgxpC7gmrj4fbCcDi.vv0SLGUw6puRijfxP57sl4FzVVRXDdq', '../img/hypnotize.png', '../img/user.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, 'Pas de résumé renseigné.'),
(12, 0, 'aaa@aaa.fr', 'aaa', '2018-05-03', 'aaa', 'aaa', '$2y$10$SUQzk4.fgJi.mVxNzU8yK.oPukb1Pqi7RTJBGoqDDHBCD1LIB4HaS', '../img/hypnotize.png', '../img/user.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, '');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id_com` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `contenu` tinytext NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id_com`),
  KEY `id_auteur` (`id_auteur`),
  KEY `id_post` (`id_post`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `emploi`
--

DROP TABLE IF EXISTS `emploi`;
CREATE TABLE IF NOT EXISTS `emploi` (
  `id_emploi` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `intitule` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `salaire` mediumint(9) NOT NULL,
  `entreprise` varchar(255) NOT NULL,
  PRIMARY KEY (`id_emploi`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

DROP TABLE IF EXISTS `formation`;
CREATE TABLE IF NOT EXISTS `formation` (
  `id_formation` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `ecole` varchar(255) NOT NULL,
  `date_deb` date NOT NULL,
  `date_fin` date NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`id_formation`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`id_formation`, `id_auteur`, `ecole`, `date_deb`, `date_fin`, `Description`) VALUES
(1, 7, 'ECE', '2018-03-01', '2018-03-02', 'oui');

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id_auteur` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  KEY `id_post` (`id_post`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `metier`
--

DROP TABLE IF EXISTS `metier`;
CREATE TABLE IF NOT EXISTS `metier` (
  `id_metier` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `poste` varchar(255) NOT NULL,
  `societe` varchar(255) NOT NULL,
  `date_deb` date NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_metier`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `metier`
--

INSERT INTO `metier` (`id_metier`, `id_auteur`, `poste`, `societe`, `date_deb`, `description`) VALUES
(1, 7, 'PDG', 'La Terre Entière', '1997-05-12', 'tmtc');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `contenu_texte` text NOT NULL,
  `date_post` datetime NOT NULL,
  `contenu_media` varchar(255) NOT NULL,
  `lieu_post` varchar(255) NOT NULL,
  `visibilite` int(11) NOT NULL,
  PRIMARY KEY (`id_post`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
