-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 05 mai 2018 à 20:12
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `piscine`
--

-- --------------------------------------------------------

--
-- Structure de la table `amis`
--

DROP TABLE IF EXISTS `amis`;
CREATE TABLE IF NOT EXISTS `amis` (
  `statut` int(11) NOT NULL,
  `fk1` int(11) NOT NULL,
  `fk2` int(11) NOT NULL,
  `user_action` int(11) NOT NULL,
  PRIMARY KEY (`fk1`,`fk2`),
  KEY `id1` (`fk1`),
  KEY `id2` (`fk2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `amis`
--

INSERT INTO `amis` (`statut`, `fk1`, `fk2`, `user_action`) VALUES
(1, 7, 9, 7),
(0, 7, 8, 7),
(1, 11, 7, 7),
(1, 7, 10, 7),
(1, 11, 12, 11),
(1, 11, 8, 11),
(1, 7, 13, 13),
(1, 16, 13, 13),
(1, 18, 16, 16);

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

DROP TABLE IF EXISTS `auteur`;
CREATE TABLE IF NOT EXISTS `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` tinyint(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `last_co` date NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `bg_img` varchar(255) NOT NULL,
  `pp_img` varchar(255) NOT NULL,
  `cv_img` blob NOT NULL,
  `resume` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `auteur`
--

INSERT INTO `auteur` (`id`, `admin`, `email`, `pseudo`, `last_co`, `prenom`, `nom`, `password`, `bg_img`, `pp_img`, `cv_img`, `resume`) VALUES
(17, 0, 'igor@ece.fr', 'igz', '2018-05-05', 'Igor', 'Fidalgo', '$2y$10$Y/22ilH3KjosC2Y5Vi.UqejsEQ0aEVxVjboO6eQc3KrFxcET1c8RG', 'prod/img/background/hypnotize.png', 'prod/img/default/user.png', 0x70726f642f696d672f64656661756c742f63765f64656661756c742e706e67, 'Je veux faire SI!'),
(18, 0, 'momo@ece.fr', 'Morgane', '2018-05-05', 'Morgane', 'Figard', '$2y$10$X6HEtTrCimf2Lu8wbBjMc.PQ6pXFnmV3.afSWBL1ivKaD565MeK0q', 'prod/img/background/sakura.png', 'prod/img/media/avatar5.png', 0x70726f642f696d672f64656661756c742f63765f64656661756c742e706e67, 'Etudiante en école d\'ingénieur'),
(16, 0, 'max@ece.fr', 'Maxou', '2018-05-05', 'Max', 'Lutz', '$2y$10$A/jCCscV2qDHwI39cQLii.K6vb21WMtc8Dvj9zOc7GQgSRl7tA92a', 'prod/img/background/hypnotize.png', 'prod/img/default/user.png', 0x70726f642f696d672f64656661756c742f63765f64656661756c742e706e67, ''),
(13, 1, 'hugo@ece.fr', 'hugo', '2018-05-05', 'Hugo', 'Calanca', '$2y$10$Z0lpfDZMmdfa5K5MPqItie/DIpRzvZr7uMSyAZgWlmdxsfPzKyoRO', 'prod/img/background/hypnotize.png', 'prod/img/media/avatar2.png', 0x2e2e2f696d672f63765f64656661756c742e706e67, '+ que Morgane');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id_com` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `contenu` tinytext NOT NULL,
  `time_stamp` datetime NOT NULL,
  PRIMARY KEY (`id_com`),
  KEY `id_auteur` (`id_auteur`),
  KEY `id_post` (`id_post`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `emploi`
--

DROP TABLE IF EXISTS `emploi`;
CREATE TABLE IF NOT EXISTS `emploi` (
  `id_emploi` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `intitule` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `salaire` mediumint(9) NOT NULL,
  `entreprise` varchar(255) NOT NULL,
  PRIMARY KEY (`id_emploi`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `emploi`
--

INSERT INTO `emploi` (`id_emploi`, `id_auteur`, `type`, `intitule`, `description`, `salaire`, `entreprise`) VALUES
(1, 13, 0, 'CDI', 'ghgh', 1000000, 'YOlo'),
(5, 13, 0, 'Stage découverte', 'Découverte', 0, 'ECE');

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

DROP TABLE IF EXISTS `formation`;
CREATE TABLE IF NOT EXISTS `formation` (
  `id_formation` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `ecole` varchar(255) NOT NULL,
  `date_deb` date NOT NULL,
  `date_fin` date NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`id_formation`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`id_formation`, `id_auteur`, `ecole`, `date_deb`, `date_fin`, `Description`) VALUES
(1, 7, 'ECE', '2018-03-01', '2018-03-02', 'oui'),
(2, 16, 'ECE Paris', '2018-05-01', '2018-05-26', 'Master'),
(3, 17, 'Igloo', '2018-05-02', '2018-05-25', 'Il fait froid!'),
(5, 18, 'UMPC', '2014-09-01', '2015-12-31', 'Médecine');

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id_auteur` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  KEY `id_post` (`id_post`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `likes`
--

INSERT INTO `likes` (`id_auteur`, `id_post`) VALUES
(13, 28),
(13, 31),
(16, 33);

-- --------------------------------------------------------

--
-- Structure de la table `metier`
--

DROP TABLE IF EXISTS `metier`;
CREATE TABLE IF NOT EXISTS `metier` (
  `id_metier` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `poste` varchar(255) NOT NULL,
  `societe` varchar(255) NOT NULL,
  `date_deb` date NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_metier`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `metier`
--

INSERT INTO `metier` (`id_metier`, `id_auteur`, `poste`, `societe`, `date_deb`, `description`) VALUES
(1, 7, 'PDG', 'La Terre Entière', '1997-05-12', 'tmtc'),
(2, 13, ' President', ' ECE Inter', '2018-05-01', 'Je suis le plus fort! '),
(3, 16, 'Jongleur', 'Troupe du soleil', '2018-05-01', 'J\'aime jongler!'),
(4, 16, 'Archaniste', 'L\'université', '2018-05-03', 'Kvothe'),
(5, 17, 'Caissier', 'ECE Paris', '2018-05-03', 'Sympa'),
(6, 18, 'VP Asso', 'ECE Inter', '2018-05-03', 'yolo');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `id_auteur` int(11) NOT NULL,
  `contenu_texte` text NOT NULL,
  `date_post` datetime NOT NULL,
  `contenu_media` varchar(255) NOT NULL,
  `lieu_post` varchar(255) NOT NULL,
  `visibilite` int(11) NOT NULL,
  PRIMARY KEY (`id_post`),
  KEY `id_auteur` (`id_auteur`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id_post`, `id_auteur`, `contenu_texte`, `date_post`, `contenu_media`, `lieu_post`, `visibilite`) VALUES
(33, 13, 'Bonjour cher réseau !', '2018-05-05 19:54:10', '', '', 0),
(28, 16, 'J\'aime les manèges', '2018-05-05 14:56:41', '', ' - à devant E2', 0),
(29, 17, 'J\'aime les arbres', '2018-05-05 15:00:11', '', '', 0),
(30, 18, 'J\'aime la nature!', '2018-05-05 15:03:17', '', ' - à forêt', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
