<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include("head.php"); ?>
    <title>Problème - ECE Network</title>
  </head>

  <body class="no_access">
    <div class="login_container">
      <div class="login_panel panel-default col-lg-4 col-md-6 col-sm-6">
        <div class="no_access_title text-center">Oups !</div>
        <div class="no_access_body text-center">
          <h3>Un problème est survenu.</h3>
          <a href="index.php" class="btn btn-primary btn-lg" role="button">Retourner à la page d'accueil</a>
        </div>
      </div>
    </div>
  </body>
</html>
