<!-- vérification de la connexion de l'utilisateur -->
<?php 
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();
  $req_notif = $bdd->prepare("SELECT count(statut) FROM amis WHERE (fk1=:id_current_user OR fk2=:id_current_user) AND statut=0 AND user_action!=:id_current_user");
  $req_notif->execute(array(
  'id_current_user' => $user['id']));
  $nbre_notif = $req_notif->fetch();

    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include("head.php"); ?>
    <title>Profil - ECE Network</title>
  </head>

  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
      	<!-- top navigation -->
        
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="index.php" class="nav-text">Accueil</a></li>
                <li><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="notifications.php" class="nav-text"><i class="fa fa-bell-o"></i>
                
                <?php 
                if($nbre_notif['count(statut)'] != 0)
                {
                  ?>
                <span class="badge badge-notify"> <?php echo $nbre_notif['count(statut)'] ?></span></a></li>
                <?php
                }
                ?>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li class="active"><a href="profil.php" class="nav-text">Mon profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
        </nav>
        <!-- /top navigation -->
      	<!-- page content -->
        <div class="row">
          <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <div class="panel panel-default">
              <div class="panel-body">
                <table class="table" style="border: none !important;">
                  <thead>
                    <tr>
                      <td class="col-lg-1 col-md-1 col-sm-1"><img src="<?php echo $user['pp_img'] ?>" height="130" width="130" style="border-radius: 50%;"></td>
                      <?php
                        $req_poste = $bdd->prepare('SELECT poste, societe FROM metier WHERE id_auteur = :id_auteur ORDER BY date_deb DESC');
                        $req_poste->execute(array('id_auteur' => $user['id']));

                        $poste = $req_poste->fetch();
                        ?>
                      <td style="vertical-align: middle;"><h2><?php echo $user['prenom']; ?> <?php echo $user['nom']; ?></h2><h4><?php echo $poste['poste']; ?> <small><?php echo $poste['societe']; ?></small></h4></td>
                    </tr>
                  </thead>
                  <tbody class="profil-table">                    
                    <tr>
                      <td class="col-lg-1 col-md-1 col-sm-1" style="text-align: center;"><h4>Résumé</h4></td>
                      <td>
                        <textarea readonly="true" disabled="true" class="form-control" style="resize: none; background-color: white;" rows="4"><?php
                            if($user['resume']=="") {
                                echo "Pas de résumé renseigné.";
                              }
                              else {
                                echo $user['resume'];
                              }
                              ?></textarea>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <!--</div>
        <div class="container-fluid">-->
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="panel panel-default">
              <div class="panel-body list-group">
                <div class="list-group-item">
                  <!--<p style="text-align: center;"><button class="btn btn-primary form-control" style="word-wrap:break-word; white-space: normal; height: 100%;">Modifier le profil</button></p>-->
                  <p style="text-align: center;"><a href="modifier_profil.php" class="btn btn-primary form-control" role="button" style="word-wrap:break-word; white-space: normal; height: 100%;">Modifier le profil</a></p>
                </div>
                <?php
                  if($user['admin']==1) {
                    ?>
                      <div class="list-group-item">
                        <p style="text-align: center;"><a href="formulaireadmin.php" class="btn btn-info form-control" role="button" style="word-wrap:break-word; white-space: normal; height: 100%;">Accéder à la page administrateur</a></p>
                      </div>
                    <?php
                  }
                ?>
                <div class="list-group-item">
                  <p style="text-align: center;"><button class="btn btn-default form-control" style="word-wrap:break-word; white-space: normal; height: 100%;">Afficher le CV</button></p>
                </div>
                <div class="list-group-item">
                  <p style="text-align: center;"><button class="btn btn-default form-control" style="word-wrap:break-word; white-space: normal; height: 100%;">Télécharger le CV</button></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Expérience professionnelle</h2>
              </div>
              <div class="list-group-item">
                <ul class="list-group">
                  <?php
                    $req_metier = $bdd->prepare('SELECT * FROM metier WHERE id_auteur = :id_auteur ORDER BY date_deb DESC');
                    $req_metier->execute(array('id_auteur' => $user['id']));

                    while($metier = $req_metier->fetch()) {
                      ?>
                        <li class="list-group-item">
                          <h3><?php echo $metier['poste']; ?> <small><?php echo $metier['societe']; ?> - <?php echo date('M Y', strtotime($metier['date_deb'])); ?></small></h3>
                          <p>"<?php echo $metier['description']; ?>"</p>
                        </li>
                      <?php
                    }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Formation</h2>
              </div>
              <div class="list-group-item">
                <ul class="list-group">
                  <?php
                    $req_formation = $bdd->prepare('SELECT * FROM formation WHERE id_auteur = :id_auteur ORDER BY date_deb DESC');
                    $req_formation->execute(array('id_auteur' => $user['id']));

                    while($formation = $req_formation->fetch()) {
                      ?>
                        <li class="list-group-item">
                          <h3><?php echo $formation['ecole']; ?><small> - <?php echo date('M Y', strtotime($formation['date_deb'])) . ' to ' . date('M Y', strtotime($formation['date_fin'])); ?></small></h3>
                          <p>"<?php echo $formation['Description']; ?>"</p>
                        </li>
                      <?php
                    }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body>
  </html>
<?php 
 
  }

  ?>