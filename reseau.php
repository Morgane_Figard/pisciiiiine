<!-- vérification de la connexion de l'utilisateur -->
<?php 
session_start();

if(!isset($_SESSION['pseudo'])) {
    //echo "accès refusé ta mère, tu t'es pas co t'as cru tu pouvais venir sur le site";
  include("acces_refuse.php");
}
else {
  $pseudo = $_SESSION['pseudo'];
  $pwd = $_SESSION['pwd'];

  //connexion à la base de données
  try {
      $bdd = new PDO('mysql:host=localhost;dbname=piscine;charset=utf8', 'root', '');
  }
  catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
  }

  $req = $bdd->prepare('SELECT * FROM auteur WHERE pseudo = :pseudo AND password = :pwd');
  $req->execute(array(
      'pseudo' => $pseudo,
      'pwd' => $pwd));

  $user = $req->fetch();
  $req_notif = $bdd->prepare("SELECT count(statut) FROM amis WHERE (fk1=:id_current_user OR fk2=:id_current_user) AND statut=0 AND user_action!=:id_current_user");
  $req_notif->execute(array(
  'id_current_user' => $user['id']));
  $nbre_notif = $req_notif->fetch();

    ?>

<html lang="en">
  <head>
  	<?php include("head.php"); ?>
    <title>Réseau - ECE Network</title>
  </head>

  <body style="background-image: url('<?php echo $user['bg_img'];?>');">
    <div class="container body">
      <div class="main_container">
      	<!-- top navigation -->
      	<?php //include("top_nav.php"); ?>
        
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <a class="navbar-brand" href="index.php"><span class="nav-text-title">ECE Network</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a href="index.php" class="nav-text">Accueil</a></li>
                <li class="active"><a href="reseau.php" class="nav-text">Réseau</a></li>
                <li><a href="emploi.php" class="nav-text">Emploi</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="notifications.php" class="nav-text"><i class="fa fa-bell-o"></i>
                
                <?php 
                if($nbre_notif['count(statut)'] != 0)
                {
                  ?>
                <span class="badge badge-notify"> <?php echo $nbre_notif['count(statut)'] ?></span></a></li>
                <?php
                }
                ?>
                <li><a href="#" class="nav-text"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="profil.php" class="nav-text">Mon profil</a></li>
                <li><a href="controleur_deconnexion.php" class="nav-text"><i class="fa fa-power-off"></i></a></li>
              </ul>
            </div>
        </nav>
        <!-- /top navigation -->
      	<!-- page content -->
        <div class="container-fluid">
          <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              <form method="post" class="navbar-form">
                <div class="input-group col-xs-12">
                  <input class="form-control" placeholder="Rechercher une personne..." name="srch-term" id="srch-term" type="text">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
        <?php
        if(!isset($_POST['srch-term']) || $_POST['srch-term']=="")
        {
          $req = $bdd->query('SELECT * FROM auteur ORDER BY nom ASC');

          while($utilisateurs = $req->fetch())
          {
            $prenom = $utilisateurs['prenom'];
            $nom = $utilisateurs['nom'];
            $id_utilisateur = $utilisateurs['id'];
            //echo $id_utilisateur;
            //requête pour vérifier les relations d'amitiés entre utilisateurs
            $check = $bdd->prepare('SELECT * FROM amis WHERE fk1=:id_current_user AND fk2=:id_utilisateur
            OR fk1=:id_utilisateur AND fk2=:id_current_user');
            $check->execute(array(
              'id_current_user' => $user['id'],
              'id_utilisateur' => $id_utilisateur));
            $check_friend = $check->fetch();
            //requête pour récupérer les informations sur les emplois des utilisateurs
            $req_emploi = $bdd->prepare('SELECT id_auteur,poste,societe FROM metier JOIN auteur ON metier.id_auteur=auteur.id WHERE metier.id_auteur=:id_utilisateur');
            $req_emploi->execute(array('id_utilisateur' => $id_utilisateur));
            $infos_utilisateur = $req_emploi->fetch();
        ?>

            <div class="container-fluid">

                <div class="col-md-8 col-md-offset-2">
                    <?php
                  if($id_utilisateur != $user['id'])
                  { 
                  ?>
                  <div class="panel panel-default">
                    <div class="list-group-item">
                        <p><b><a href="profil_ami.php?id_ami=<?php echo $id_utilisateur; ?>"><img src="<?php echo $utilisateurs['pp_img'] ?>" height="45" width="45" style="border-radius: 50%;">  <?php echo $prenom." ".$nom; ?></a></b>
                          <?php 
                          if($check_friend['statut'] == null)
                          {
                            ?>
                              <a href="controleur_requete.php?id_current_user=<?php echo $user['id']?>&id_utilisateur=<?php echo $id_utilisateur?>">
                              <button class="btn btn-primary pull-right">Ajouter
                              <i class="fa fa-plus" style="size:22px;"></i> 

                              </button>
                              </a>
                                <?php
                          }else
                            if($check_friend['statut'] == 0) 
                           { 
                            ?> 
                            <i class="fa fa-hourglass-half pull-right" style="color:sienna; size:22px;"></i>
                           <?php 
                           }
                         
                          if($check_friend['statut'] == 1) 
                          {
                          ?>
                          <i class="fa fa-check pull-right" style="color:green; size:22px;"></i>
                          <?php 
                          }
                          if($check_friend['statut'] == 2) 
                          {
                          ?>
                          <i class="fa fa-ban pull-right" style="color:red; size:22px;"></i>
                          <?php 
                          }                                     
                          ?>
                        </p>
                        <?php 
                        if($infos_utilisateur)
                        { 
                          ?>
                          <h6><?php echo $infos_utilisateur['poste']." à ".$infos_utilisateur['societe']; ?></h6>
                        <?php 
                        }
                        ?>
                      </div>
                  </div>
                      <?php
                    }
                  ?>
                    
                </div>
            </div>
            <?php
            }
          }
           else
          {
            $contenu_recherche = htmlspecialchars($_POST['srch-term']);
            $recherche_utilisateur = $bdd->prepare('SELECT * FROM auteur WHERE nom LIKE :terme_recherche OR prenom LIKE :terme_recherche');
            $recherche_utilisateur->execute(array(
              'terme_recherche' => '%' . $contenu_recherche . '%'));


            while($utilisateur = $recherche_utilisateur->fetch())
            {
               if(!$utilisateur)
            {
              ?>
              <div class="container-fluid">
                <div class="col-md-8">
                  <div class="panel panel-default">
                    <div class="list-group-item">
                      <p>Aucun résultat </p>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              break;
            }
            $id_utilisateur = $utilisateur['id'];
            $check = $bdd->prepare('SELECT * FROM amis WHERE fk1=:id_current_user AND fk2=:id_utilisateur
            OR fk1=:id_utilisateur AND fk2=:id_current_user');
            $check->execute(array(
              'id_current_user' => $user['id'],
              'id_utilisateur' => $id_utilisateur));
            $check_friend = $check->fetch();
            $req_emploi = $bdd->prepare('SELECT poste,societe FROM metier JOIN auteur ON metier.id_auteur=auteur.id WHERE metier.id_auteur=:id_utilisateur');
            $req_emploi->execute(array('id_utilisateur' => $id_utilisateur));
            $infos_emploi = $req_emploi->fetch();
          ?>
              <div class="container-fluid">
                <div class="col-md-8 col-md-offset-2">
                  <div class="panel panel-default">
              
                    <div class="list-group-item">
                      <p><b><a href="profil_ami.php?id_ami=<?php echo $id_utilisateur; ?>"><?php echo $utilisateur['prenom']." ".$utilisateur['nom']; ?></a></b>
                        <?php 
                          if($check_friend['statut'] == null && $id_utilisateur != $user['id'])
                          {  
                          ?> 
                          <a href="controleur_requete.php?id_current_user=<?php echo $user['id']?>&id_utilisateur=<?php echo $id_utilisateur?>">
                              <button class="btn btn-primary pull-right">Ajouter
                              <i class="fa fa-plus" style="size:22px;"></i> 
                              </button>
                          </a>
                        
                          <?php 
                          }
                          else if($check_friend['statut'] == 0 && $id_utilisateur != $user['id']) 
                          {
                          ?>
                              <i class="fa fa-hourglass-half pull-right" style="color:sienna; size:22px;"></i>                   
                         <?php 
                          }
                           if($check_friend['statut'] == 1) 
                          {
                          ?>
                          <i class="fa fa-check pull-right" style="color:green; size:22px;"></i>
                          <?php 
                          }
                          if($check_friend['statut'] == 2) 
                          {
                          ?>
                          <i class="fa fa-ban pull-right" style="color:red; size:22px;"></i>
                          <?php 
                          }                                     
                          ?>
                      </p>

                      <?php 
                      if($infos_emploi)
                      { 
                        ?>
                        <h6><?php echo $infos_emploi['poste']." à ".$infos_emploi['societe']; ?></h6>
                    <?php 
                      }
                    ?>
                    </div>
                  </div>
                </div>
            </div>
          <?php
            }
          }
          ?>
        <!-- /page content -->
      </div>
    </div>
  </body>
  </html>

<?php
  }
  ?>
